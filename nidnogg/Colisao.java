/*/////////////////////////////////////////////////////////////////////////////
 *                                                                            *
 * Universidade Federal do Rio de Janeiro - Laboratórios 2 e 3                *
 * Aluno: Henrique Vermelho de Toledo                                         *
 * DRE: 116076549                                                             *
 * Esse jogo é uma adaptação do popular "Androids" em Java.                   *                                                                          *
 * Compilado no terminal com:                                                 *
alias run="javac *.java && java Jogo" && run                                  *
 * Aqui encontra-se a classe de colisão.                                      *
 */////////////////////////////////////////////////////////////////////////////
public class Colisao {
    //Verifica colisão entre tiros e asteróides.
    public static boolean tiroAsteroide(Asteroide asteroide, Tiro tiro) {
        return Math.sqrt( Math.pow(asteroide.x - tiro.x, 2 ) + Math.pow(asteroide.y - tiro.y, 2)) < asteroide.tamanho * 10;

    }

    //Verifica colião entre a nave e asteróides.
    public static boolean naveAsteroide(Asteroide asteroide, Nave nave) {
        return Math.sqrt( Math.pow(asteroide.x - nave.x, 2 ) + Math.pow(asteroide.y - nave.y, 2)) < asteroide.tamanho * 10 + 5;
    }

}
