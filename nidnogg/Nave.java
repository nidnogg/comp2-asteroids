/*/////////////////////////////////////////////////////////////////////////////
 *                                                                            *
 * Universidade Federal do Rio de Janeiro - Laboratórios 2 e 3                *
 * Aluno: Henrique Vermelho de Toledo                                         *
 * DRE: 116076549                                                             *
 * Esse jogo é uma adaptação do popular "Androids" em Java.                   *                                                                          *
 * Compilado no terminal com:                                                 *
alias run="javac *.java && java Jogo" && run                                  *
 * Aqui encontra-se a classe de naves.                                        *
 */////////////////////////////////////////////////////////////////////////////
public class Nave {
    double x;
    double y;
    double velVetor;
    /* Para acessar as componentes do velVetor
     * X -> usa-se velVetor * cos(angulo)
     * Y -> usa-se velVetor * sin(angulo)
     * Isso substitui o uso de velX, e velY. Assim fica mais preciso o incremento
     * do valor do vetor.
     */
    double angulo = 0;
    boolean flag = false;
    int alt = 60, larg = 50;

    public Nave(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public int getAltura() {
        return 600;
    }

    public int getLargura() {
        return 800;
    }

    public void acelerar(double dt) {
        velVetor = velVetor + 100*dt;
    }
    public void desacelerar(double dt) {
        velVetor = velVetor - 100*dt;
    }

    public void mover(double dt) {

        //Movimento
        x += Math.cos(angulo) * velVetor * dt;
        y += Math.sin(angulo) * velVetor * dt;

        //Aqui a nave é ignorada nessa posição. Essa coordenada é usada
        //pra sumir com a mesma.
        if(x == -3000 && y == -3000) {
            return;
        }

        //Faz com que a nave reapareça nos cantos da tela ao ultrapassar os limites.
        if(x <= 0) x = getLargura() - larg;
        if(x >= getLargura()) x = 0;
        if(y <= 0) y = getAltura() - alt;
        if(y >= getAltura()) y = 0;


    }

    //Mudança de direção ao teclar.
    public void esquerda(double dt) {
        angulo -= Math.PI * dt; //saporra tá em radiano

    }

    public void direita(double dt) {
        angulo += Math.PI * dt;
    }

    public void desenhar(Tela t, int xarq) {
    //xarq = 135 nave ligada xarq = 70 pra nave desligada;
        int yarq = 0;
        t.imagem("naves.png", xarq, yarq, larg, alt-10, (angulo+Math.PI/2), x, y);

    }

    public Tiro atirar() {
        return new Tiro(x + larg/2, y + alt/2, 500, angulo);
    }

}
