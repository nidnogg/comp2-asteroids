import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
/*/////////////////////////////////////////////////////////////////////////////
 *                                                                            *
 * Universidade Federal do Rio de Janeiro - Laboratórios 2 e 3                *
 * Aluno: Henrique Vermelho de Toledo                                         *
 * DRE: 116076549                                                             *
 * Esse jogo é uma adaptação do popular "Androids" em Java.                   *                                                                          *
 * Compilado no terminal com:                                                 *
alias run="javac *.java && java Jogo" && run                                  *
 * Aqui encontra-se a main.                                                   *
 */////////////////////////////////////////////////////////////////////////////

 /*
  * CONTROLES
  * Up -> Acelerar
  * Down -> Desacelerar
  * Left/Right -> Girar para respectivas direções
  * P -> pausa
  */
public class Jogo {

    //Variáveis principais
    int pontos = 0;
    int vidas = 3;
    int acertos = 0;

    double tempo = 0;

    //Os hashsets de Asteroides e de Tiros possuem dois hashsets complementares
    //para remoção e adição dos mesmos.
    //Solução adaptada para uma excepção enconrada na tentativa de remoção durante iteração.
    Set<Asteroide> asteroides = new HashSet<Asteroide>();
    Set<Asteroide> addAsteroides = new HashSet<Asteroide>();
    Set<Asteroide> removAsteroides = new HashSet<Asteroide>();
    Set<Tiro> tiros = new HashSet<Tiro>();
    Set<Tiro> removTiros = new HashSet<Tiro>();

    boolean pausa, gameOver = false;
    boolean naveLigada = false;
    boolean criarNave = true;

    Nave nave = new Nave(getLargura()/2 - 35, getAltura()/2 - 40);

    public Jogo() {
        //Criação dos 6 asteróides iniciais.
        for(int i = 0; i < 5; i++) {
            asteroides.add(new Asteroide());
        }
    }

    public String getTitulo() {
        return "Asteroids";
    }

    public int getLargura() {
        return 800;
    }
    public int getAltura() {
        return 600;
    }

    //Loop principal do jogo.
    public void tique(Set<String> keys, double dt) {
        if(pausa || gameOver) return;

        //Função tempo.
        delay(dt);

        for(Asteroide asteroide : asteroides) {
            asteroide.mover(dt);
        }

        for(Tiro tiro : tiros) {
            tiro.mover(dt);
            if(tiro.isRemovido) {
                removTiros.add(tiro);
            }
        }

        //Aqui itera-se sobre asteroides e tiros, para verificar se colidiram.
        //Também trata-se a geração de novos asteroides menores conforme o tamanho,
        //após a destruição dos mesmos.
        for(Iterator<Asteroide> i = asteroides.iterator(); i.hasNext();) {
            Asteroide asteroide = i.next();

            for(Iterator<Tiro> j = tiros.iterator(); j.hasNext();) {
                Tiro tiro = j.next();
                if(Colisao.tiroAsteroide(asteroide, tiro)) {

                    if(asteroide.tamanho == 1) {
                        removAsteroides.add(asteroide);
                    }

                    if(asteroide.tamanho == 2) {
                        removAsteroides.add(asteroide);
                    }

                    if(asteroide.tamanho == 3) {
                        removAsteroides.add(asteroide);
                        addAsteroides.add(new Asteroide(asteroide.x, asteroide.y, -asteroide.velX, -asteroide.velY, 1));
                        addAsteroides.add(new Asteroide(asteroide.x, asteroide.y, asteroide.velX, asteroide.velY, 1));
                    }

                    if(asteroide.tamanho == 4) {
                        removAsteroides.add(asteroide);
                        addAsteroides.add(new Asteroide(asteroide.x, asteroide.y, asteroide.velX, asteroide.velY, 2));
                        addAsteroides.add(new Asteroide(asteroide.x, asteroide.y, -asteroide.velX, -asteroide.velY, 1));
                    }
                    j.remove();
                    Motor.tocar("hit.wav");

                    //É feita a contagem de pontos.
                    pontos++;

                    //Função para adicionar asteroides conforme a destruição dos mesmos, evitando mapa vazio.
                    if(pontos > 5) {
                        if(Math.random() > 0.5 || asteroides.size() < 5.0) {
                            addAsteroides.add(new Asteroide());
                        }
                    }

                    if(asteroides.size() == 0.0) addAsteroides.add(new Asteroide());

                }
            }
        }

        //Aqui são removidos e adicionados todos os tiros e asteroides marcados.
        tiros.removeAll(removTiros);
        asteroides.addAll(addAsteroides);
        asteroides.removeAll(removAsteroides);
        //Esse for checa por colisão de nave com cada asteroide.
        for(Iterator<Asteroide> k = asteroides.iterator(); k.hasNext();) {
            Asteroide asteroideNav = k.next();
            //Detecção de colisão.
            if(Colisao.naveAsteroide(asteroideNav, nave)) {
                tempo = 0;
                vidas -= 1;
                Motor.tocar("explosion.wav");
                //Essas coordenadas são para sumir com a nave quando destruída.
                nave = new Nave(-3000,-3000);
            }
            //Tempo de demora para respawnar a nave, assim como coords de sua posição inativa (-3000, -3000)
            if(tempo > 3 && nave.x == -3000) {
                nave = new Nave(getLargura()/2 - 35, getAltura()/2 - 40);
            }

        }
        //Alguns ontroles.
        if(keys.contains("left")) nave.esquerda(dt);
        if(keys.contains("right")) nave.direita(dt);
        if(keys.contains("up")) {
            naveLigada = true;
            nave.acelerar(dt);
        } else {
            naveLigada = false;
        }

        if(keys.contains("down")) nave.desacelerar(dt);

        if(nave.x != -3000 && nave.y != -3000) {
            nave.mover(dt);
        }
    }

    public void tecla(String keys) {
        //Mais controles.
        if(keys.contains(" ") || keys.contains("down")) {
            tiros.add(nave.atirar());
        }

        if(keys.contains("p")) {
            pausa = !pausa;
        }
    }
    
    //Funções de desenho.
    public void desenhar(Tela t) {

        if(nave.x == -3000 && vidas > 0) {
            t.texto(String.format("%d", 3 - (int)tempo ), getLargura()/2, getAltura()/2, 36, Cor.VERMELHO);
        }

        if(vidas > 0) {
            t.texto(String.format("%04d", pontos), 50, 50, 36, Cor.VERMELHO);
            t.texto(String.format("%d", vidas), getLargura() - 50, 50, 36, Cor.VERMELHO);
        }

        if(pausa)
            t.texto("PAUSA", getLargura()/3, getAltura()/2, getLargura() / 10, Cor.VERMELHO);

        if(vidas > 0) {
            if(naveLigada) {
                //Recorta sprite com nave ligada.
                nave.desenhar(t, 135);
            } else {
                //Recorta sprite com nave desligada.
                nave.desenhar(t, 70);
            }

        } else {
            t.texto("GAME OVER", getLargura()/5, getAltura()/2, getLargura() / 10, Cor.VERMELHO);
        }
        //Os dois fors a seguir iteram pelos conjuntos de asteroides
        //e tiros, respectivamente.
        for(Asteroide asteroide : asteroides) {
            asteroide.desenhar(t);
        }

        for(Tiro tiro : tiros) {
            tiro.desenhar(t);
        }

    }

    //Função contadora para respawnar nave.
    public void delay(double dt) {
        tempo += dt;
    }

    public static void main(String[] args) {
        //Função main, aonde o jogo é rodado.
        new Motor(new Jogo());
        System.out.println("Starting game");
    }
}
