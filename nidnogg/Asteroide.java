import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
/*/////////////////////////////////////////////////////////////////////////////
 *                                                                            *
 * Universidade Federal do Rio de Janeiro - Laboratórios 2 e 3                *
 * Aluno: Henrique Vermelho de Toledo                                         *
 * DRE: 116076549                                                             *
 * Esse jogo é uma adaptação do popular "Androids" em Java.                   *                                                                          *
 * Compilado no terminal com:                                                 *
alias run="javac *.java && java Jogo" && run                                  *
 * Aqui encontra-se a classe de asteroides.                                   *
 */////////////////////////////////////////////////////////////////////////////
public class Asteroide {

    //Variáveis principais
    int xarq;
    int yarq;
    double x = Math.random() * 601;
    double y = Math.random() * 801;
    double velX = Math.random() * 301;
    double velY = Math.random() * 301;
    double velRot = Math.random() * (2*Math.PI + 1);
    double angulo = 0;

    //Alguns construtores.
    public Asteroide() {

    }

    public Asteroide(double x, double y, double velX, double velY, int tamanho) {
        this.x = x;
        this.y = y;
        this.velX = velX;
        this.velY = velY;
        this.tamanho = tamanho;
    }

    //Valores aleatórios para tamanho e tipo
    int tamanho = ThreadLocalRandom.current().nextInt(1, 4 + 1);
    int tipo = ThreadLocalRandom.current().nextInt(0, 7 + 1);

    //Referentes ao tamanho dos asteroides que serão recortados.
    int alt = 0, larg = 0;

    public int getAltura() {
        return 800;
    }

    public int getLargura() {
        return 600;
    }

    public void mover(double dt) {
        x += velX * dt;
        y += velY * dt;
        angulo += velRot * dt;
        //trocar 800 e 600 aqui por getLargura e getAltura
        if(x <= 0) x = getAltura() - larg;
        if(x >= getAltura()) x = 0;
        if(y <= 0) y = getLargura() - alt;
        if(y >= getLargura()) y = 0;
    }

    public void desenhar(Tela t) {
        yarq = 48 * tipo; //são as posições x e y dentro do arquivo, dependentes do tipo.
        switch(tamanho) {
            case 1:
                xarq = 0;
                alt = 16;
                larg = 16;
            case 2:
    			xarq = 16;
    			alt = 16;
    			larg = 16;
    			break;
    		case 3:
    			xarq = 32;
    			alt = 32;
    			larg = 32;
    			break;
    		case 4:
    			xarq = 64;
    			alt = 48;
    			larg = 48;
    			break;
        }
        //Desenho do asteroide.
        t.imagem("asteroids.png", xarq, yarq, larg, alt, angulo, x, y); //0.0 é o angulo de inclinação da imagem
    }


    /* Uma outra maneira de declarar números aleatórios. via random, pra futura referência.

                        Random rand = new Random();
                        int  n = rand.nextInt(50) + 1;

       Codigo inutilizado com o mesmo.

                Asteroide.x =  Math.random() * 600 + 1;
                Asteroide.y =  Math.random() * 800 + 1;
                Asteroide.velX = Math.random() * 300 + 1;
                Asteroide.velY = Math.random() * 300 + 1;
                Asteroide.velRot = Math.random() * (2*Math.PI) + 1;*/
}
