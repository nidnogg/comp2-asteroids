/*/////////////////////////////////////////////////////////////////////////////
 *                                                                            *
 * Universidade Federal do Rio de Janeiro - Laboratórios 2 e 3                *
 * Aluno: Henrique Vermelho de Toledo                                         *
 * DRE: 116076549                                                             *
 * Esse jogo é uma adaptação do popular "Androids" em Java.                   *                                                                          *
 * Compilado no terminal com:                                                 *
alias run="javac *.java && java Jogo" && run                                  *
 * Aqui encontra-se a classe de tiros.                                        *
 */////////////////////////////////////////////////////////////////////////////
public class Tiro {
    //Variáveis principais
    double x;
    double y;
    double velVetor;
    double angulo = 0;
    boolean isRemovido = false;
    Cor cor = Cor.BRANCO;

    public Tiro(double x, double y, double velVetor, double angulo) {
        this.x = x;
        this.y = y;
        this.velVetor = velVetor;
        /* para acessar as componentes
         * X -> usa-se velVetor * cos(angulo)
         * Y -> usa-se velVetor * sin(angulo)
         */
        this.angulo = angulo;
    }

    public void desenhar(Tela t) {
        t.circulo(this.x, y, 2, cor);
    }
    //Aqui faz a verificação e remoção dos tiros que ultrapassam os limites da tela.
    public void mover(double dt) {
        x += Math.cos(angulo) * velVetor * dt;
        if(x >= 800) {
            isRemovido = true;
        }

        if(x < 0) {
            isRemovido = true;
        }

        y += (double) Math.sin(angulo) * velVetor * dt;
        if(y >= 600) {
            isRemovido = true;
        }

        if(y < 0) {
            isRemovido = true;
        }
    }
}
